# services/users/src/api/__init__.py


from flask_restx import Api

from src.api.ping import ping_namespace
from src.api.auth import auth_namespace
from src.api.users.users import users_namespace
from src.api.photos.controllers.photos import photos_namespace
from src.api.photos.controllers.single_photo import singles_namespace
from src.api.photos.controllers.multi_photos import multi_namespace
from src.api.photos.controllers.public_photos import public_namespace
from src.api.photos.controllers.view_photo import view_namespace
from src.api.photos.controllers.photo_profiles import profiles_namespace

api = Api(version="1.0", title="Users API", doc="/doc")

api.add_namespace(ping_namespace, path="/ping")
api.add_namespace(auth_namespace, path="/auth")
api.add_namespace(users_namespace, path="/users")
api.add_namespace(photos_namespace, path="/photos")
api.add_namespace(singles_namespace, path="/photos")
api.add_namespace(multi_namespace, path="/photos")
api.add_namespace(public_namespace, path="/photos")
api.add_namespace(view_namespace, path="/photos")
api.add_namespace(profiles_namespace, path="/photos")
