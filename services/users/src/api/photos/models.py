from sqlalchemy.sql import func
from src import db


class Photo(db.Model):

    __tablename__ = "photos"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    path = db.Column(db.String(128), nullable=False)
    title = db.Column(db.String(128), nullable=False)
    caption = db.Column(db.String(255), nullable=False)
    public = db.Column(db.Boolean(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey("users.id"), nullable=False)
    user = db.relationship("User", foreign_keys=[user_id], backref='photos')
    created_date = db.Column(db.DateTime, default=func.now(), nullable=False)

    def __init__(self, user_id, path, title="untitled", caption="", public=True):
        self.path = path
        self.title = title
        self.caption = caption
        self.public = public
        self.user_id = user_id
