import os
import string
from random import choice
from flask import current_app
from src import db
from werkzeug.utils import secure_filename
from src.api.photos.models import Photo
from src.api.users.crud import get_user_by_id

relative = "/usr/src/app/resources/user_images/"


def add_image(image, title, caption, public, user_id):
    """
    Add image to database
    :param image: Image file to save
    :param title: Title of the image
    :param caption: Caption for the image
    :param public: True if the image can be viewed by other users. Otherwise, false
    :param user_id: User id of the image owner
    :return: image object
    """
    if image is None:
        return None
    if get_user_by_id(user_id) is None:
        return None
    path = __save_image(image)
    if path is None:
        return None
    photo = Photo(path=path, user_id=user_id, title=title, caption=caption, public=public)
    db.session.add(photo)
    db.session.commit()
    return photo


def add_image_without_details(image, public, user_id):
    """
    Add image to database without a title or caption
    :param image: Image file to save
    :param public: True if the image can be viewed by other users. Otherwise, false
    :param user_id: User id of the image owner
    :return: image object
    """
    if image is None:
        return None
    if get_user_by_id(user_id) is None:
        return None
    path = __save_image(image)
    if path is None:
        return None
    photo = Photo(path=path, user_id=user_id, public=public)
    db.session.add(photo)
    db.session.commit()
    return photo


def __save_image(image):
    """
    Save the image to disc
    :param image: Image file to save
    :return: path the image was saved at
    """
    path = __name_file(image)
    if path is None:
        return None
    path_complete = os.path.join(path[0], path[1], path[2])
    if os.path.exists(path_complete):
        path = __name_file(image)
        if os.path.exists(path_complete):
            return None
    image.save(path_complete)
    return path[2]


def __name_file(image):
    """
    Create a random name for the image before saving to avoid issue with duplicate names
    :param image: Image file to save
    :return: the path to save the image at
    """
    # can't use Path class methods
    filename = __randomize_filename(image.filename)

    if filename == "" or (not __allowed_file(filename)):
        return None
    path = (
        os.getcwd(),
        current_app.config["USER_IMAGES_FOLDER"],
        secure_filename(filename),
    )

    return path


def __allowed_file(filename):
    """
    Checks if the given file is of a valid file type
    :param filename: Name of the file (including the extension)
    :return: True if the the image is of a valid file type
    """
    return "." in filename and filename.rsplit(".", 1)[
        1
    ].lower() in current_app.config.get("ALLOWED_EXTENSIONS")


def __randomize_filename(filename):
    """
    Create a random filename to avoid saving files with duplicate names
    :param filename: Given filename from the user
    :return: The new random filename
    """
    letters = string.ascii_lowercase
    new_name = "".join(choice(letters) for i in range(10))
    return new_name + "-" + filename.lower()


def delete_image(image):
    """
    Delete an image from the database
    :param image: Image to delete
    :return: Deleted image
    """
    if image is None:
        return None
    path = relative + image.path
    if os.path.exists(path):
        os.remove(path)
    db.session.delete(image)
    db.session.commit()
    return image


def edit_image(image, title, caption, public):
    """
    Edit an image's details
    :param image: Image to edit
    :param title: New image title
    :param caption: New image caption
    :param public: True if the image can be viewed by other users. Otherwise, false
    :return: The updated image object
    """
    image.title = title
    image.caption = caption
    image.public = public
    db.session.commit()
    return image


def get_image_by_id(photo_id):
    """
    Retrieve an image by its id if it exists in the database and if the saved path exists
    :param photo_id: Id of the image to retrieve
    :return: Image object if it exists, otherwise null
    """
    photo = Photo.query.filter_by(id=photo_id).first()
    if photo is None:
        return None
    if path_exists(relative + photo.path):
        return photo
    else:
        delete_image(photo)
        return None


def get_public_images_by_user_id(user_id):
    """
    Retrieve all of the public images for a user's profile
    :param user_id: User id of the user whose images you'd like to retrieve
    :return: List of public images owned by given user_id.
    """
    return Photo.query.filter_by(user_id=user_id, public=True).all()


def path_exists(path):
    """Inject static method to help with unit testing
    :param path: Path to check if it exists on disk
    """
    return os.path.exists(path)
