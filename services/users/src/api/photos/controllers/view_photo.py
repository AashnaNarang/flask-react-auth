from flask import send_from_directory
from flask_restx import Resource, Namespace

view_namespace = Namespace("photos")


class ViewPhoto(Resource):
    # @view_namespace.expect(parser)
    @view_namespace.response(200, "<Image File>")
    @view_namespace.response(404, "Could not find image")
    # @view_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    # @view_namespace.response(403, "Token required")
    # @view_namespace.response(404, "Invalid api token")
    def get(self, filename):
        # authorize()
        dirin = "/usr/src/app/resources/user_images"
        try:
            return send_from_directory(dirin, filename)
        except Exception as e:
            print(e)
            view_namespace.abort(404, "Could not find image")


view_namespace.add_resource(ViewPhoto, "-view/<string:filename>")
