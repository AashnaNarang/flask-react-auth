from flask_restx import Resource, fields, Namespace
from src.api.users.crud import get_user_by_id
from src.api.photos.controllers.authorize import authorize
from src.api.photos.crud import get_public_images_by_user_id


public_namespace = Namespace("photos")

photo = public_namespace.model(
    "Photo",
    {
        "id": fields.Integer(readOnly=True),
        "path": fields.String(required=True),
        "title": fields.String(required=True),
        "caption": fields.String(required=True),
        "public": fields.Boolean(required=True),
        "user_id": fields.Integer(required=True),
        "created_date": fields.DateTime(required=True),
    },
)

get_parser = public_namespace.parser()
get_parser.add_argument("Authorization", required=True, location="headers")
get_parser.add_argument("id", type=int, required=True, location='form')


class PublicPhoto(Resource):
    @public_namespace.marshal_with(photo, as_list=True)
    @public_namespace.expect(get_parser)
    @public_namespace.response(200, "<List of Photos>")
    @public_namespace.response(400, "Selected photo is an invalid file type")
    @public_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @public_namespace.response(403, "Token required")
    @public_namespace.response(404, "Invalid api token")
    @public_namespace.response(404, "User with id <user_id> does not exist")
    def post(self):
        """Get all of the public photos for a user
        :param id: Id of the user you would like to view their public photos
        """
        args = get_parser.parse_args()
        user_id = args["id"]

        authorize(public_namespace)
        if get_user_by_id(user_id) is None:
            public_namespace.abort(404, f"User with id {user_id} does not exist")

        return get_public_images_by_user_id(user_id), 200


public_namespace.add_resource(PublicPhoto, "-public")
