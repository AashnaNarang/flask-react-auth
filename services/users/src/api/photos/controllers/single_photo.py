from flask_restx import Resource, fields, Namespace
from src.api.photos.controllers.authorize import authorize
from src.api.photos.crud import get_image_by_id

singles_namespace = Namespace("photos")

photo = singles_namespace.model(
    "Photo",
    {
        "id": fields.Integer(readOnly=True),
        "path": fields.String(required=True),
        "title": fields.String(required=True),
        "caption": fields.String(required=True),
        "public": fields.Boolean(required=True),
        "user_id": fields.Integer(required=True),
        "created_date": fields.DateTime(required=True),
    },
)

get_parser = singles_namespace.parser()
get_parser.add_argument("Authorization", required=True, location="headers")
get_parser.add_argument("id", type=int, required=True, location='form')


class SingleImage(Resource):
    @singles_namespace.marshal_with(photo)
    @singles_namespace.expect(get_parser)
    @singles_namespace.response(200, "<Photo>")
    @singles_namespace.response(400, "Selected photo is an invalid file type")
    @singles_namespace.response(401, "You do not have access to view this image")
    @singles_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @singles_namespace.response(403, "Token required")
    @singles_namespace.response(404, "Invalid api token")
    @singles_namespace.response(404, "Photo <photo_id> does not exist")
    def post(self):
        """Get single photo by photo_id
        :param id: Id of the image you would like to retrieve
        """
        args = get_parser.parse_args()
        photo_id = args["id"]

        user = authorize(singles_namespace)
        photo = get_image_by_id(photo_id)
        if photo is None:
            singles_namespace.abort(404, f"Photo with id {photo_id} does not exist")
        if photo.public or (photo.user_id == user.id):
            return photo, 200
        else:
            singles_namespace.abort(401, "You do not have access to view this image")


singles_namespace.add_resource(SingleImage, "-single")