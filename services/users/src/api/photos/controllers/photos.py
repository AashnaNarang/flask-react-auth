from flask_restx import Resource, fields, Namespace, inputs
from werkzeug.datastructures import FileStorage
from src.api.photos.crud import (
    add_image,
    delete_image,
    get_image_by_id,
    edit_image
)
from src.api.photos.controllers.authorize import authorize

photos_namespace = Namespace("photos")

photo = photos_namespace.model(
    "Photo",
    {
        "id": fields.Integer(readOnly=True),
        "path": fields.String(required=True),
        "title": fields.String(required=True),
        "caption": fields.String(required=True),
        "public": fields.Boolean(required=True),
        "user_id": fields.Integer(required=True),
        "created_date": fields.DateTime(required=True),
    },
)

parser = photos_namespace.parser()
parser.add_argument("Authorization", required=True, location="headers")

edit_parser = parser.copy()
edit_parser.add_argument("id", type=int, required=True, location='form')
edit_parser.add_argument("public", type=inputs.boolean, required=True, location='form')
edit_parser.add_argument("title", type=str, required=True, location='form')
edit_parser.add_argument("caption", type=str, required=True, location='form')

delete_parser = parser.copy()
delete_parser.add_argument("id", type=int, required=True, location='form', action='append')

upload_parser = parser.copy()
upload_parser.add_argument("image", location="files", type=FileStorage, required=True)
upload_parser.add_argument("public", type=inputs.boolean, required=True, location='form')
upload_parser.add_argument("title", type=str, required=True, location='form')
upload_parser.add_argument("caption", type=str, required=True, location='form')


class Photo(Resource):
    @photos_namespace.marshal_with(photo)
    @photos_namespace.expect(upload_parser)
    @photos_namespace.response(200, "Photo was added!")
    @photos_namespace.response(400, "Selected photo is an invalid file type")
    @photos_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @photos_namespace.response(403, "Token required")
    @photos_namespace.response(404, "Invalid api token")
    def post(self):
        """Add a photo to user's profile
        :param image: Image file to add to your profile
        :param title: Title for the image
        :param caption: Caption for the image
        :param public: True if the image can be viewed by other users. Otherwise, false
        """
        args = upload_parser.parse_args()
        img = args["image"]
        title = args["title"]
        caption = args["caption"]
        public = args["public"]

        user = authorize(photos_namespace)
        photo = add_image(img, title, caption, public, user.id)
        if not photo:
            photos_namespace.abort(400, "Selected photo is an invalid file type. Allowed file types include png, jpg, jpeg, gif")
        return photo, 200

    @photos_namespace.expect(delete_parser)
    @photos_namespace.response(200, "Photo(s) were removed")
    @photos_namespace.response(400, "Selected photo is an invalid file type")
    @photos_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @photos_namespace.response(403, "Token required")
    @photos_namespace.response(404, "Invalid api token")
    @photos_namespace.response(404, "Photo(s) do not exist")
    def delete(self):
        """Delete a photo from user's profile
        :param id: list of one or more ids of photos you would like to remove from your profile"""
        args = delete_parser.parse_args()
        photo_ids = args["id"]
        response_object = {}

        user = authorize(photos_namespace)
        removed = ""
        not_removed = ""
        for photo_id in photo_ids:
            photo = get_image_by_id(photo_id)
            if (photo is None) or (photo.user_id is not user.id):
                not_removed += f"{photo_id},"
            else:
                photo = delete_image(photo)
                if photo is None:
                    not_removed += f"{photo_id},"
                else:
                    removed += f" {photo.title},"
        if removed != "":
            response_object["message"] = f"The following photo(s) were removed:{removed[:-1]}."
            if not_removed != "":
                response_object["message"] += f" The photo(s) with following ids, {not_removed[:-1]}, were not removed because you do not have permission to perform this action or they do not exist"
            return response_object, 200
        else:
            response_object["message"] = f"The selected photo(s) were not removed because you do not have permission to perform this action or they do not exist"
            return response_object, 404

    @photos_namespace.marshal_with(photo)
    @photos_namespace.expect(edit_parser)
    @photos_namespace.response(200, "Photo details were updated")
    @photos_namespace.response(400, "Selected photo is an invalid file type")
    @photos_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @photos_namespace.response(403, "Token required")
    @photos_namespace.response(404, "Invalid api token")
    @photos_namespace.response(404, "Photo does not exist")
    def put(self):
        """Edit a photo from user's profile
        :param id: Id of the image you would like to edit
        :param title: Title for the image
        :param caption: Caption for the image
        :param public: True if the image can be viewed by other users. Otherwise, false
        """
        args = edit_parser.parse_args()
        photo_id = args["id"]
        title = args["title"]
        caption = args["caption"]
        public = args["public"]

        user = authorize(photos_namespace)
        photo = get_image_by_id(photo_id)
        if photo is None:
            photos_namespace.abort(404, "Photo does not exist")
        if photo.user_id is not user.id:
            photos_namespace.abort(401, "You do not have permission to edit this image")
        photo = edit_image(photo, title, caption, public)
        return photo, 200, {"message": f"Photo {photo.id} was updated"}


photos_namespace.add_resource(Photo, "")
