from src.api.photos.controllers.authorize import authorize
from src.api.users.crud import get_all_users
from flask_restx import Resource, fields, Namespace
from src.api.photos.crud import get_public_images_by_user_id

profiles_namespace = Namespace("photos")

parser = profiles_namespace.parser()
parser.add_argument("Authorization", required=True, location="headers")

photo = profiles_namespace.model(
    "Photo",
    {
        "id": fields.Integer(readOnly=True),
        "path": fields.String(required=True),
        "title": fields.String(required=True),
        "caption": fields.String(required=True),
        "public": fields.Boolean(required=True),
        "user_id": fields.Integer(required=True),
        "created_date": fields.DateTime(required=True),
    },
)

profile = profiles_namespace.model(
    "Profile",
    {
        "id": fields.Integer(required=True),
        "username": fields.String(required=True),
        "photos": fields.Nested(photo)
    },
)


class PhotoProfiles(Resource):
    @profiles_namespace.marshal_with(profile, as_list=True)
    @profiles_namespace.expect(parser)
    @profiles_namespace.response(200, "<List of Profiles>")
    @profiles_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @profiles_namespace.response(403, "Token required")
    @profiles_namespace.response(404, "Invalid api token")
    def get(self):
        """Get all public photos for all users
        """
        authorize(profiles_namespace)
        profiles = []
        for user in get_all_users():
            data = {}
            data["photos"] = get_public_images_by_user_id(user.id)
            data["username"] = user.username
            data["id"] = user.id
            profiles.append(data)
        return profiles, 200


profiles_namespace.add_resource(PhotoProfiles, "-profiles")
