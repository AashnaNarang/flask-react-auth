from flask_restx import Resource, fields, Namespace, inputs
from src.api.photos.controllers.authorize import authorize
from werkzeug.datastructures import FileStorage
from flask import request, current_app
from src.api.photos.crud import add_image

multi_namespace = Namespace("photos")

photo = multi_namespace.model(
    "Photo",
    {
        "id": fields.Integer(readOnly=True),
        "path": fields.String(required=True),
        "title": fields.String(required=True),
        "caption": fields.String(required=True),
        "public": fields.Boolean(required=True),
        "user_id": fields.Integer(required=True),
        "created_date": fields.DateTime(required=True),
    },
)

parser = multi_namespace.parser()
parser.add_argument("Authorization", required=True, location="headers")

upload_multi_parser = parser.copy()
upload_multi_parser.add_argument("images[]", location="files", type=FileStorage, required=True)
upload_multi_parser.add_argument("public", type=inputs.boolean, required=True, location='form')


class MultiPhoto(Resource):
    @multi_namespace.marshal_with(photo, as_list=True)
    @multi_namespace.expect(upload_multi_parser)
    @multi_namespace.response(200, "Photo(s) were added!")
    @multi_namespace.response(400, "Photo(s) were not added because they are invalid file types")
    @multi_namespace.response(400, "Selected photo is an invalid file type")
    @multi_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @multi_namespace.response(403, "Token required")
    @multi_namespace.response(404, "Invalid api token")
    def post(self):
        """Add 1 or more photos to user's profile
        :param images[]: list of image files to add to your profile
        :param public: True if the image can be viewed by other users. Otherwise, false
        """
        args = upload_multi_parser.parse_args()
        imgs = request.files.getlist("images[]")
        public = args["public"]

        user = authorize(multi_namespace)
        photos = []
        not_added = ""
        for img in imgs:
            filename = img.filename
            photo = add_image(img, filename.split(".", 1)[0], "", public, user.id)
            if not photo:
                not_added += f"{filename},"
            else:
                photos.append(photo)
        if not_added != "":
            allowed_file_types = ", ".join(current_app.config.get("ALLOWED_EXTENSIONS"))
            message = f"The following file(s), {not_added} were not added because they are an invalid file type. Allowed file types include {allowed_file_types}."
            if len(photos) == 0:
                return photos, 400, {"message": message}
        else:
            message = "Photos were succesfully added"

        return photos, 200, {"message": message}

    @multi_namespace.marshal_with(photo, as_list=True)
    @multi_namespace.expect(parser)
    @multi_namespace.response(200, "<List of photos>")
    @multi_namespace.response(400, "Selected photo is an invalid file type")
    @multi_namespace.response(401, "Signature expired or invalid token. Please log in again.")
    @multi_namespace.response(403, "Token required")
    @multi_namespace.response(404, "Invalid api token")
    def get(self):
        """Get all of your photos"""
        user = authorize(multi_namespace)
        return user.photos, 200


multi_namespace.add_resource(MultiPhoto, "-multi")
