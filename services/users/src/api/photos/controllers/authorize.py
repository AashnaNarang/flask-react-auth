import jwt
from flask import request
from src.api.users.crud import get_user_by_id, decode_token


def authorize(api_namespace):
    auth_header = request.headers.get("Authorization")
    if auth_header:
        try:
            access_token = auth_header.split(" ")[1]
            user = get_user_by_id(decode_token(access_token))
            if not user:
                api_namespace.abort(404, "Invalid api token")
            return user
        except jwt.ExpiredSignatureError:
            api_namespace.abort(401, "Signature expired. Please log in again.")
        except jwt.InvalidTokenError:
            api_namespace.abort(401, "Invalid token. Please log in again.")
    else:
        api_namespace.abort(403, "Token required")
    return None
