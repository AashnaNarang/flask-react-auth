import jwt
import json
from io import BytesIO
import src.api.photos.controllers.photos
from src.api.photos.crud import get_image_by_id, delete_image
from src.tests import invalid_image_path, valid_image_path, login


def test_add_image_to_user_profile(test_app, test_database, add_user):
    user1 = add_user("username", "email@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "image": (BytesIO(b'image'), valid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo = get_image_by_id(data["id"])

    assert resp.status_code == 200
    assert photo is not None
    assert "valid_image.png" in data["path"]
    assert "title" == data["title"]
    assert "caption" == data["caption"]
    assert data["public"]
    assert user1.id == data["user_id"]
    delete_image(photo)


def test_add_invalid_image_type(test_app, test_database, add_user):
    user1 = add_user("username2", "email2@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "image": (BytesIO(b'image'), invalid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 400
    assert "Selected photo is an invalid file type. Allowed file types include png, jpg, jpeg, gif" in data["message"]


def test_add_image_invalid_user_id(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username3", "email3@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "image": (BytesIO(b'image'), invalid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_add_image_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "image": (BytesIO(b'image'), valid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_add_image_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.post(
        "/photos",
        headers={"Authorization": "Bearer expiredtoken"},
        data={
            "image": (BytesIO(b'image'), invalid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_add_image_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos",
        headers={"Authorization": ""},
        data={
            "image": (BytesIO(b'image'), invalid_image_path),
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]
