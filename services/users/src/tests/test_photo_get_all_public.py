import json
import jwt
from PIL import Image
import src.api.photos.controllers.photos
from src.tests import valid_image_path, login
from src.api.photos.crud import delete_image, add_image


def test_get_all_public_images_from_user_profile(test_app, test_database, add_user):
    user1 = add_user("username17", "email17@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", False, user1.id)

    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": user1.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["id"] == photo.id
    assert data[1]["id"] == photo2.id
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_get_all_public_images_from_user_profile_not_signed_into(test_app, test_database, add_user):
    user1 = add_user("username18", "email18@email.com", "password")
    user2 = add_user("username19", "email19@email.com", "password")
    client = test_app.test_client()
    api_token = login(user2, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", False, user1.id)

    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": user1.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert len(data) == 2
    assert data[0]["id"] == photo.id
    assert data[1]["id"] == photo2.id
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_get_public_images_invalid_user_id_from_token(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username20", "email20@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": user1.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_get_all_public_images_invalid_user_from_id(test_app, test_database, add_user):
    user1 = add_user("username21", "email21@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", False, user1.id)

    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": -100,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "User with id -100 does not exist" in data["message"]
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_get_public_images_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_get_public_images_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.post(
        "/photos-public",
        headers={"Authorization": "Bearer expiredtoken"},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_get_public_images_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-public",
        headers={"Authorization": ""},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]
