import jwt
import json
from io import BytesIO
import src.api.photos.controllers.photos
from src.api.photos.crud import get_image_by_id, delete_image
from src.tests import invalid_image_path, valid_image_path, login


def test_add_images_to_user_profile(test_app, test_database, add_user):
    user1 = add_user("username13", "email13@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "images[]": [(BytesIO(b'image'), valid_image_path),(BytesIO(b'image'), valid_image_path),(BytesIO(b'image'), valid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )

    data = json.loads(resp.data.decode())
    photo = get_image_by_id(data[0]["id"])
    photo2 = get_image_by_id(data[1]["id"])
    photo3 = get_image_by_id(data[2]["id"])

    assert resp.status_code == 200
    assert "Photos were succesfully added" in resp.headers["message"]
    assert photo is not None
    assert user1.id == photo.user_id
    assert photo.public
    assert photo2 is not None
    assert user1.id == photo2.user_id
    assert photo2.public
    assert photo3 is not None
    assert user1.id == photo3.user_id
    assert photo3.public
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_add_images_given_one_invalid_type(test_app, test_database, add_user):
    user1 = add_user("username14", "email14@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "images[]": [(BytesIO(b'image'), valid_image_path),(BytesIO(b'image'), invalid_image_path),(BytesIO(b'image'), valid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo = get_image_by_id(data[0]["id"])
    photo2 = get_image_by_id(data[1]["id"])

    assert len(data) == 2
    assert "The following file(s), /usr/src/app/src/tests/invalid_file.jfif, ", '\
    "were not added because they are an invalid file type. Allowed file types include png, gif,", '\
    "jpg, jpeg." in resp.headers["message"]
    assert resp.status_code == 200
    assert photo is not None
    assert user1.id == photo.user_id
    assert photo.public
    assert photo2 is not None
    assert user1.id == photo2.user_id
    assert photo2.public
    delete_image(photo)
    delete_image(photo2)


def test_add_images_given_all_invalid_type(test_app, test_database, add_user):
    user1 = add_user("username15", "email15@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "images[]": [(BytesIO(b'image'), invalid_image_path),(BytesIO(b'image'), invalid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())

    assert len(data) == 0
    assert "The following file(s), /usr/src/app/src/tests/invalid_file.jfif, /usr/src/app/src/tests/invalid_file.jfif, ", '\
    "were not added because they are an invalid file type. Allowed file types include png, gif,", '\
    "jpg, jpeg." in resp.headers["message"]
    assert resp.status_code == 400


def test_add_images_invalid_user_id(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username16", "email16@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "images[]": [(BytesIO(b'image'), invalid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_add_images_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "images[]": [(BytesIO(b'image'), valid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_add_images_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": "Bearer expiredtoken"},
        data={
            "images[]": [(BytesIO(b'image'), valid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_add_images_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-multi",
        headers={"Authorization": ""},
        data={
            "images[]": [(BytesIO(b'image'), valid_image_path)],
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]