from src.api.photos.crud import (
    add_image,
    add_image_without_details,
    delete_image,
    edit_image,
    get_image_by_id,
    get_public_images_by_user_id
)
import src.api.photos.crud
from src.api.photos.models import Photo
from PIL import Image
import os

relative_path = os.path.abspath(os.path.dirname(__file__))
valid_image_path = os.path.join(
    relative_path,
    "valid_image.png",
)
invalid_image_path = os.path.join(
    relative_path,
    "invalid_file.jfif",
)


def test_add_image_saves_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    photo_from_db = get_image_by_id(photo.id)
    assert photo is photo_from_db
    assert photo.title == photo_from_db.title
    assert photo.caption == photo_from_db.caption
    assert photo.public == photo_from_db.public
    assert photo.user_id == photo_from_db.user_id
    assert "valid_image.png" in photo_from_db.path
    assert os.path.exists("/usr/src/app/resources/user_images/" + photo_from_db.path)
    delete_image(photo)


def test_add_image_given_invalid_file_type(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(invalid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    assert photo is None


def test_add_image_given_none_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    photo = add_image(None, "test title", "test caption", True, user.id)
    assert photo is None


def test_add_image_given_invalid_user_id(test_app, test_database, add_user):
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, -100)
    assert photo is None


def test_add_image_without_details_saves_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image_without_details(img, True, user.id)
    photo_from_db = get_image_by_id(photo.id)
    assert photo is photo_from_db
    assert photo_from_db.title == "untitled"
    assert photo_from_db.caption == ""
    assert photo.public == photo_from_db.public
    assert photo.user_id == photo_from_db.user_id
    assert "valid_image" in photo_from_db.path
    assert "png" in photo_from_db.path
    assert os.path.exists("/usr/src/app/resources/user_images/" + photo_from_db.path)
    delete_image(photo)


def test_add_image_without_details_given_invalid_file_type(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(invalid_image_path)
    photo = add_image_without_details(img, True, user.id)
    assert photo is None


def test_add_image_without_details_given_none_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    photo = add_image_without_details(None, True, user.id)
    assert photo is None


def test_add_image_without_details_given_invalid_user_id(test_app, test_database, add_user):
    img = Image.open(valid_image_path)
    photo = add_image_without_details(img, True, -100)
    assert photo is None


def test_delete_image_removes_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    delete_image(photo)
    assert os.path.exists(photo.path) == False
    assert get_image_by_id(photo.id) is None


def test_edit_image_updates_image(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    edit_image(photo, "new title", "new caption", False)
    photo_from_db = get_image_by_id(photo.id)
    assert photo_from_db.id == photo.id
    assert photo_from_db.title == "new title"
    assert photo_from_db.caption == "new caption"
    assert photo_from_db.public == False
    assert photo_from_db.path == photo.path
    delete_image(photo)


def test_get_image_by_id(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    result = get_image_by_id(photo.id)
    assert result is photo
    assert result.title == photo.title
    assert result.caption == photo.caption
    assert result.public == photo.public
    assert result.user_id == photo.user_id
    delete_image(photo)


def test_get_image_by_id_given_path_does_not_exist_deletes_item_in_db(test_app, test_database, add_user, monkeypatch):
    def mock_path_exists(path):
        return None

    monkeypatch.setattr(src.api.photos.crud, "path_exists", mock_path_exists)
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    result = get_image_by_id(photo.id)
    assert result is None
    assert Photo.query.filter_by(id=photo.id).first() is None
    delete_image(photo)


def test_get_public_images_by_user_id(test_app, test_database, add_user):
    user = add_user("justatest", "test@test.com", "greaterthaneight")
    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user.id)
    photo2 = add_image(img, "test title2", "test caption2", False, user.id)
    result = get_public_images_by_user_id(user.id)
    assert len(result) == 1
    assert photo2 not in result
    assert result[0] is photo
    assert result[0].title == photo.title
    assert result[0].caption == photo.caption
    assert result[0].public == True
    assert result[0].user_id == photo.user_id
    delete_image(photo)
    delete_image(photo2)
