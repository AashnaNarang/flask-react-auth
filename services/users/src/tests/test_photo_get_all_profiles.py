import json
import jwt
from PIL import Image
from src import db
import src.api.photos.controllers.photos
from src.tests import valid_image_path, login
from src.api.photos.crud import delete_image, add_image
from src.api.photos.models import Photo
from src.api.users.models import User


def test_get_all_profiles_returns_public_photos_only(test_app, test_database, add_user):
    db.session.query(User).delete()
    db.session.query(Photo).delete()
    user1 = add_user("username28", "email28@email.com", "password")
    user2 = add_user("username29", "email29@email.com", "password")
    client = test_app.test_client()
    api_token = login(user2, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", False, user1.id)

    resp = client.get(
        "/photos-profiles",
        headers={"Authorization": "Bearer " + api_token},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data[0]["photos"][0]["id"] == photo.id
    assert data[0]["photos"][1]["id"] == photo2.id
    assert len(data[0]["photos"]) == 2
    assert data[0]["id"] == user1.id
    assert data[0]["username"] == user1.username
    assert len(data[1]["photos"]) == 0
    assert data[1]["id"] == user2.id
    assert data[1]["username"] == user2.username
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_gets_all_profiles_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.get(
        "/photos-profiles",
        headers={"Authorization": "Bearer invalidToken"},
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_get_images_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.get(
        "/photos-profiles",
        headers={"Authorization": "Bearer expiredtoken"},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_get_images_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.get(
        "/photos-profiles",
        headers={"Authorization": ""},
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]