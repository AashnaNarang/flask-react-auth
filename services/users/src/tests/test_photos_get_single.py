import json
import jwt
from PIL import Image
import src.api.photos.controllers.photos
from src.tests import valid_image_path, login
from src.api.photos.crud import delete_image, add_image


def test_get_single_image_by_photo_id(test_app, test_database, add_user):
    user1 = add_user("username22", "email22@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)

    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": photo.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["id"] == photo.id
    assert data["title"] == photo.title
    assert data["caption"] == photo.caption
    assert data["public"] == photo.public
    assert data["path"] == photo.path
    assert data["user_id"] == photo.user_id
    delete_image(photo)


def test_get_single_image_given_id_to_private_image(test_app, test_database, add_user):
    user1 = add_user("username23", "email23@email.com", "password")
    user2 = add_user("username24", "email24@email.com", "password")
    client = test_app.test_client()
    api_token = login(user2, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", False, user1.id)

    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": photo.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert data["message"] == "You do not have access to view this image"
    delete_image(photo)


def test_get_single_private_image_from_your_profile(test_app, test_database, add_user):
    user1 = add_user("username25", "email25@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", False, user1.id)
    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": photo.id,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert data["id"] == photo.id
    assert data["title"] == photo.title
    assert data["caption"] == photo.caption
    assert data["public"] == photo.public
    assert data["path"] == photo.path
    assert data["user_id"] == photo.user_id
    delete_image(photo)


def test_get_single_image_invalid_user_id_from_token(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username26", "email26@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": 1,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_get_single_image_invalid_photo_id(test_app, test_database, add_user):
    user1 = add_user("username27", "email27@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": -100,
        },
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Photo with id -100 does not exist" in data["message"]


def test_get_single_images_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_get_single_image_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.post(
        "/photos-single",
        headers={"Authorization": "Bearer expiredtoken"},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_get_single_image_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.post(
        "/photos-single",
        headers={"Authorization": ""},
        data={
            "id": 1,
        }
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]
