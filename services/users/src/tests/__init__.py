import os
import json

relative_path = os.path.abspath(os.path.dirname(__file__))
valid_image_path = os.path.join(
    relative_path,
    "valid_image.png",
)
invalid_image_path = os.path.join(
    relative_path,
    "invalid_file.jfif",
)


def login(user, client):
    resp_login = client.post(
        "/auth/login",
        data=json.dumps({"email": user.email, "password": "password"}),
        content_type="application/json",
    )
    return json.loads(resp_login.data.decode())["api_token"]