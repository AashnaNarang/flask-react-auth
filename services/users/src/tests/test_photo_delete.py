import json
import jwt
from PIL import Image
import src.api.photos.controllers.photos
from src.tests import valid_image_path, login
from src.api.photos.crud import get_image_by_id, delete_image, add_image


def test_delete_single_image_from_user_profile(test_app, test_database, add_user):
    user1 = add_user("username4", "email4@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)

    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": [photo.id]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo_from_db = get_image_by_id(photo.id)

    assert resp.status_code == 200
    assert "The following photo(s) were removed: test title." in data["message"]
    assert photo_from_db is None
    delete_image(photo)


def test_delete_multiple_image_from_user_profile(test_app, test_database, add_user):
    user1 = add_user("username5", "email5@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", True, user1.id)

    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": [photo.id, photo2.id, photo3.id]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo_from_db = get_image_by_id(photo.id)
    photo_from_db2 = get_image_by_id(photo2.id)
    photo_from_db3 = get_image_by_id(photo3.id)

    assert resp.status_code == 200
    assert "The following photo(s) were removed: test title, test title, test title." in data["message"]
    assert photo_from_db is None
    assert photo_from_db2 is None
    assert photo_from_db3 is None
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_delete_multiple_image_from_user_profile_given_one_invalid(test_app, test_database, add_user):
    user1 = add_user("username6", "email6@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    photo2 = add_image(img, "test title", "test caption", True, user1.id)
    photo3 = add_image(img, "test title", "test caption", True, user1.id)

    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": [photo.id, photo2.id, -100, photo3.id]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo_from_db = get_image_by_id(photo.id)
    photo_from_db2 = get_image_by_id(photo2.id)
    photo_from_db3 = get_image_by_id(photo3.id)

    assert resp.status_code == 200
    assert "The following photo(s) were removed: test title, test title, test title. ", '\
        "The photo(s) with following ids, -100, were not removed because you do not have permission ", '\
        "to perform this action or they do not exist" in data["message"]
    assert photo_from_db is None
    assert photo_from_db2 is None
    assert photo_from_db3 is None
    delete_image(photo)
    delete_image(photo2)
    delete_image(photo3)


def test_delete_image_invalid_user_id(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username7", "email7@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": [1]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_delete_image_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "id": [1]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_delete_image_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.delete(
        "/photos",
        headers={"Authorization": "Bearer expiredToken"},
        data={
            "id": [1]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_delete_image_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.delete(
        "/photos",
        headers={"Authorization": ""},
        data={
            "id": [1]
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]
