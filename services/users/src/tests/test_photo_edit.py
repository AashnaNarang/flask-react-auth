import json
import jwt
from PIL import Image
import src.api.photos.controllers.photos
from src.tests import valid_image_path, login
from src.api.photos.crud import get_image_by_id, delete_image, add_image


def test_edit_image_in_user_profile(test_app, test_database, add_user):
    user1 = add_user("username8", "email8@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)
    resp = client.put(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": photo.id,
            "title": "new title",
            "caption": "new caption",
            "public": False,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    photo = get_image_by_id(data["id"])

    assert resp.status_code == 200
    assert photo is not None
    assert "valid_image.png" in data["path"]
    assert "new title" == data["title"]
    assert "new caption" == data["caption"]
    assert not data["public"]
    assert user1.id == data["user_id"]
    delete_image(photo)


def test_edit_image_that_does_not_exist(test_app, test_database, add_user):
    user1 = add_user("username9", "email9@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.put(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": -100,
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Photo does not exist" in data["message"]


def test_edit_image_invalid_user_id(test_app, test_database, add_user, monkeypatch):
    def mock_get_user_by_id(user_id):
        return None

    monkeypatch.setattr(src.api.photos.controllers.authorize, "get_user_by_id", mock_get_user_by_id)

    user1 = add_user("username10", "email10@email.com", "password")
    client = test_app.test_client()
    api_token = login(user1, client)
    resp = client.put(
        "/photos",
        headers={"Authorization": "Bearer " + api_token},
        data={
            "id": 1,
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Invalid api token" in data["message"]


def test_edit_image_invalid_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.put(
        "/photos",
        headers={"Authorization": "Bearer invalidtoken"},
        data={
            "id": 1,
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())

    assert resp.status_code == 401
    assert "Invalid token. Please log in again." in data["message"]


def test_edit_image_expired_api_token(test_app, test_database, add_user, monkeypatch):
    def mock_decode_token(token):
        raise jwt.ExpiredSignatureError

    monkeypatch.setattr(src.api.photos.controllers.authorize, "decode_token", mock_decode_token)

    client = test_app.test_client()
    resp = client.put(
        "/photos",
        headers={"Authorization": "Bearer expiredtoken"},
        data={
            "id": 1,
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 401
    assert "Signature expired. Please log in again." in data["message"]


def test_edit_image_given_no_api_token(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.put(
        "/photos",
        headers={"Authorization": ""},
        data={
            "id": 1,
            "title": "title",
            "caption": "caption",
            "public": True,
        },
        content_type="multipart/form-data",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 403
    assert "Token required" in data["message"]
