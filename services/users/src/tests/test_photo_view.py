from PIL import Image
import json
from src.tests import valid_image_path
from src.api.photos.crud import delete_image, add_image


def test_view_image(test_app, test_database, add_user):
    user1 = add_user("username30", "email30@email.com", "password")
    client = test_app.test_client()

    img = Image.open(valid_image_path)
    photo = add_image(img, "test title", "test caption", True, user1.id)

    resp = client.get(
        f"/photos-view/{photo.path}",
    )
    assert(type(resp.data) == bytes)
    assert resp.status_code == 200
    delete_image(photo)


def test_view_image_invalid_image_name(test_app, test_database, add_user):
    client = test_app.test_client()
    resp = client.get(
        "/photos-view/randomfile.png",
    )
    data = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert "Could not find image" in data["message"]
