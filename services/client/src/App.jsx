import React, { Component } from "react";
import axios from "axios";
import { Route, Switch } from "react-router-dom";
import "./App.css"

import ProfileList from "./components/ProfileList";
import Profile from "./components/Profile";
import NavBar from "./components/NavBar";
import LoginForm from "./components/LoginForm";
import RegisterForm from "./components/RegisterForm";
import UserStatus from "./components/UserStatus";


class App extends Component {
  constructor() {
    super();

    this.state = {
      users: [],
      profiles: [],
      title: "Candid.",
      accessToken: null,
      apiToken: null,
      messageType: null,
      messageText: null,
    };
  }

  componentDidMount = () => {
    this.isAuthenticated();
  };

  createMessage = (type, text) => {
    this.setState({
      messageType: type,
      messageText: text,
    });
    setTimeout(() => {
      this.removeMessage();
    }, 3000);
  };

  removeMessage = () => {
    this.setState({
      messageType: null,
      messageText: null,
    });
  };


  getProfiles = () => {
    axios
      .get(`${process.env.REACT_APP_API_SERVICE_URL}/photos-profiles`, {
        headers: {
          'Authorization': `Bearer ${this.state.apiToken}`
        }
      })
      .then((res) => {
        this.setState({ profiles: res.data });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  handleCloseModal = () => {
    this.setState({ showModal: false });
  };

  handleLoginFormSubmit = (data) => {
    const url = `${process.env.REACT_APP_API_SERVICE_URL}/auth/login`;
    axios
      .post(url, data)
      .then((res) => {
        this.setState({
          accessToken: res.data.access_token,
          apiToken: res.data.api_token
        });
        this.getProfiles();
        window.localStorage.setItem("refreshToken", res.data.refresh_token);
        this.createMessage("success", "You have logged in successfully.");
      })
      .catch((err) => {
        console.log(err);
        this.createMessage("danger", "Incorrect email and/or password.");
      });
  };

  handleOpenModal = () => {
    this.setState({ showModal: true });
  };

  handleRegisterFormSubmit = (data) => {
    const url = `${process.env.REACT_APP_API_SERVICE_URL}/auth/register`;
    axios
      .post(url, data)
      .then((res) => {
        this.createMessage("success", "You have registered successfully.");
      })
      .catch((err) => {
        console.log(err);
        this.createMessage("danger", "That user already exists.");
      });
  };

  isAuthenticated = () => {
    if (this.state.accessToken || this.validRefresh()) {
      return true;
    }
    return false;
  };

  logoutUser = () => {
    window.localStorage.removeItem("refreshToken");
    this.setState({
      accessToken: null,
      apiToken: null 
    });
    this.createMessage("success", "You have logged out.");
  };

  removeMessage = () => {
    this.setState({
      messageType: null,
      messageText: null,
    });
  };

  removeUser = (user_id) => {
    axios
      .delete(`${process.env.REACT_APP_API_SERVICE_URL}/users/${user_id}`)
      .then((res) => {
        this.getUsers();
        this.createMessage("success", "User removed.");
      })
      .catch((err) => {
        console.log(err);
        this.createMessage("danger", "Something went wrong.");
      });
  };

  validRefresh = () => {
    const token = window.localStorage.getItem("refreshToken");
    if (token) {
      axios
        .post(`${process.env.REACT_APP_API_SERVICE_URL}/auth/refresh`, {
          refresh_token: token,
        })
        .then((res) => {
          this.setState({
            accessToken: res.data.access_token,
            apiToken: res.data.api_token 
          });
          this.getProfiles();
          window.localStorage.setItem("refreshToken", res.data.refresh_token);
          return true;
        })
        .catch((err) => {
          return false;
        });
    }
    return false;
  };


  render() {
    return (
      <div>
        <NavBar
          title={this.state.title}
          logoutUser={this.logoutUser}
          isAuthenticated={this.isAuthenticated}
        />
        <section className="section">
          <div className="container">
            <br />
            <Switch>
              <Route
                exact
                path="/"
                render={() => (
                  <div>
                    <h1 className="title is-1">Explore</h1>
                    <hr />
                      <ProfileList
                        profiles={this.state.profiles}
                        // isAuthenticated={this.isAuthenticated}
                      />
                  </div>
                )}
              />
              <Route 
                exact
                path="/profile"
                render={() => (
                  <Profile
                    apiToken={this.state.apiToken}
                    isAuthenticated={this.isAuthenticated}
                  />
                )}
              />
              <Route
                exact
                path="/register"
                render={() => (
                  <RegisterForm
                    // eslint-disable-next-line react/jsx-handler-names
                    handleRegisterFormSubmit={this.handleRegisterFormSubmit}
                    isAuthenticated={this.isAuthenticated}
                  />
                )}
              />
              <Route
                exact
                path="/login"
                render={() => (
                  <LoginForm
                    // eslint-disable-next-line react/jsx-handler-names
                    handleLoginFormSubmit={this.handleLoginFormSubmit}
                    isAuthenticated={this.isAuthenticated}
                  />
                )}
              />
              <Route
                exact
                path="/status"
                render={() => (
                  <UserStatus
                    accessToken={this.state.accessToken}
                    apiToken={this.state.apiToken}
                    isAuthenticated={this.isAuthenticated}
                  />
                )}
              />
            </Switch>
          </div>
        </section>
      </div>
    );
  }
}

export default App;
