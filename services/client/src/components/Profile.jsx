import React, { Component } from "react";
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel'
import PropTypes from "prop-types";
import axios from "axios";
import FormData from "form-data";
import { Redirect } from "react-router-dom";
import Gallery from 'react-grid-gallery';
import Modal from "react-modal";
import AddPhoto from "./AddPhoto"
import "./Profile.css";
import "./../App.css"

const modalStyles = {
  content: {
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    border: 0,
    background: "transparent",
  },
};

Modal.setAppElement(document.getElementById("root"));

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      apiToken: this.props.apiToken,
      images: [],
      selectAllChecked: false,
      openModal: false,
      modalTitle: "",
      addMulti: false
    };
}

componentDidMount() {
  this.refresh()
}

getMyProfile = () => {
  axios
    .get(`${process.env.REACT_APP_API_SERVICE_URL}/photos-multi`, {
      headers: {
        'Authorization': `Bearer ${this.state.apiToken}`
      }
    })
    .then((res) => {
      res.data.map((photo) => {
        photo["src"]= `${process.env.REACT_APP_API_SERVICE_URL}/photos-view/`+ photo.path;
        photo["thumbnail"]= `${process.env.REACT_APP_API_SERVICE_URL}/photos-view/`+ photo.path;
        photo["thumbnailWidth"]= 500;
        photo["thumbnailHeight"]= 300;
      })
      this.setState({
        images: res.data
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

refresh = () => {
  const token = window.localStorage.getItem("refreshToken");
  if (token) {
    axios
      .post(`${process.env.REACT_APP_API_SERVICE_URL}/auth/refresh`, {
        refresh_token: token,
      })
      .then((res) => {
        this.setState({
          apiToken: res.data.api_token 
        });
        this.getMyProfile();
        window.localStorage.setItem("refreshToken", res.data.refresh_token);
        return true;
      })
      .catch((err) => {
        return false;
      });
  }
  return false;
};

allImagesSelected (images) {
  var f = images.filter(
      function (img) {
          return img.isSelected == true;
      }
  );
  return f.length == images.length;
}


handleSelectImage = (index,image) => {
  var images = this.state.images;
  var img = images[index];
  if(img.hasOwnProperty("isSelected"))
      img.isSelected = !img.isSelected;
  else
      img.isSelected = true;

  this.setState({
      images: images,
      selectAllChecked: this.allImagesSelected(images)
  });
}

getSelectedImages = () => {
  var selected = [];
  for (var i = 0; i < this.state.images.length; i++)
      if (this.state.images[i].isSelected == true)
          selected.push(this.state.images[i]);
  return selected;
}

handleClickedSelectAll = () => {
  var selectAllChecked = !this.state.selectAllChecked;

  var images = this.state.images;
  for(var i = 0; i < this.state.images.length; i++)
    images[i].isSelected = selectAllChecked;
  this.setState({
      images: images,
      selectAllChecked: selectAllChecked
  });
}

handleCloseModal = () => {
  this.setState({
      openModal: false,
      modalTitle: "",
  });
};

handleOpenModalAddMulti = () => {
  this.setState({
    openModal: true,
    modalTitle: "Add Multiple Photos",
    addMulti: true
  });
};

handleOpenModalAddSingle = () => {
  this.setState({
    openModal: true,
    modalTitle: "Add Photo",
    addMulti: false
  });
};

addPhoto = (data) => {
  axios
    .post(`${process.env.REACT_APP_API_SERVICE_URL}/photos`, data, {
      headers: {
        'Authorization': `Bearer ${this.state.apiToken}`
      }
    })
    .then((res) => {
      this.handleCloseModal();
      this.getMyProfile();
    })
    .catch((err) => {
      console.log(err);
      this.handleCloseModal();
    });
};

addPhotos = (data) => {
  var config = {
    method: 'post',
    url: `${process.env.REACT_APP_API_SERVICE_URL}/photos-multi`,
    headers: { 
      'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2MTM0OTc4NjksImlhdCI6MTYxMDkwNTg2OSwic3ViIjoxfQ.OKc10tA7Y18ZMbGcTBXOAN1J4Zl7gT2SHjCWBGR6N90'
    },
    data : data
  };

  axios(config)
  .then(function (response) {
    console.log(JSON.stringify(response.data));
  })
  .catch(function (error) {
    console.log(error);
  });
  // axios
  //   .post(`${process.env.REACT_APP_API_SERVICE_URL}/photos-multi`, data, {
  //     headers: {
  //       'Authorization': `Bearer ${this.state.apiToken}`
  //     }
  //   })
  //   .then((res) => {
  //     this.handleCloseModal();
  //     this.refresh();
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //     this.handleCloseModal();
  //   });
};

handleDelete = () => {
  var data = new FormData();
  this.getSelectedImages().map(image => {
    data.append('id', image.id);
  });

  var config = {
    method: 'delete',
    url: `${process.env.REACT_APP_API_SERVICE_URL}/photos`,
    headers: { 
      'Authorization': `Bearer ${this.state.apiToken}`
    },
    data : data
  };

  axios(config)
  .then((res) => {
    // add popup or toast message to show successfully deleted
    })
  .catch((err) => {
    console.log(err);
  });

  var images = this.state.images;
  images.map(image => {
    image.isSelected = false;
  });
  this.setState({
    selectAllChecked: false,
    images: images
  });

  this.refresh();
}

render() {
  return (
    <div>
      <h1 className="title is-1">My Profile</h1>
      <hr />
      <div>
        <Button onClick={this.handleOpenModalAddSingle}>
          Add
        </Button>
        <Button onClick={this.handleOpenModalAddMulti}>
          Add++
        </Button>
        <Button onClick={this.handleDelete}>
          Delete
        </Button>
        <FormControlLabel
          className="selectAll"
          label="Select All"
          control={
            <Checkbox
              checked={this.state.selectAllChecked}
              onChange={this.handleClickedSelectAll}
            />
          }
        />
      </div>
      <Modal
        isOpen={this.state.openModal}
        style={modalStyles}
      >
        <div className="modal is-active">
          <div className="modal-background" />
            <div className="modal-card">
              <header className="modal-card-head">
                <p className="modal-card-title">{this.state.modalTitle}</p>
                  <button
                    className="delete"
                    aria-label="close"
                    onClick={this.handleCloseModal}
                  />
              </header>
              <section className="modal-card-body">
                <AddPhoto addMulti={this.state.addMulti} addPhoto={this.addPhoto} addPhotos={this.addPhotos} />
              </section>
            </div>
        </div>
      </Modal>
      <br />
      <Gallery images={this.state.images} onSelectImage={this.handleSelectImage} />
    </div>
    
  );
}
}

Profile.propTypes = {
  apiToken: PropTypes.string,
};

export default Profile;
