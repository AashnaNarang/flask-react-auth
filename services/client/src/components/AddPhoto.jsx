import React, { Component } from "react";
import PropTypes from "prop-types";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';

import "./form.css";

class AddPhoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      images: [],
      title: "",
      caption: "",
      public: false,
      isSubmitting: false
    };
  }

  getFileUpload() {
      if (this.props.addMulti) {
        return (
            <div>
              <label className="label" htmlFor="input-photo">
                Select Images
              </label>
              <input
                name="images[]"
                id="input-file"
                type="file"
                multiple
                accept=".gif,.png,.jpg,.jpeg"
              />
              <p>Accepted types: png, jpeg, jpg, gif</p>
            </div>
        )
      } else {
          return (
            <div>
              <label className="label" htmlFor="input-photo">
                Select Images
              </label>
              <input
                name="image"
                id="input-file"
                type="file"
                accept=".gif,.png,.jpg,.jpeg"
              />
              <p>Accepted types: png, jpeg, jpg, gif</p>
            </div>
          )
      }
  }


  handleSubmit = (data) => {
    this.setState({
      isSubmitting: true
    })
    var FormData = require('form-data');
    var fs = require('fs');
    var formData = new FormData();
    formData.append("public", this.state.public);
    if(this.props.addMulti){
        data.target[0].files.map(file => {
          formData.append('images[]', fs.createReadStream(file));
        });
        // fs.createReadStream(file.name)
        // formData.append('public', data.public);
        // formData.append("images[]", data.target[0].files);
        this.props.addPhotos(formData);
    } else {
        formData.append("title", this.state.title);
        formData.append("caption", this.state.caption);
        formData.append("image", data.target[0].files[0]);
        this.props.addPhoto(formData);
    }
    this.setState({
        isSubmitting: false,
        public: false
    })
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        {this.getFileUpload()}
        <br />
        {!this.props.addMulti && (
          <div>
            <TextField required fullWidth id="title" onChange={(e)=> this.setState({ title: e.target.value })} label="Title" placeholder="Enter image title" variant="outlined" />
            <br />
            <TextField required fullWidth id="caption" onChange={(e)=> this.setState({ caption: e.target.value })} label="Caption" variant="outlined" />
             <br />
          </div>
        )}

        <div className="field">
          <FormControlLabel
            label="Public"
            control={<Switch color="primary" onChange={()=> this.setState({ public: !this.state.public })} />}
            value={this.state.public}
            labelPlacement="start"
          />
        </div>
        <input
          type="submit"
          className="button is-primary"
          value="Submit"
          disabled={this.state.isSubmitting}
        />
      </form>
    )
  }
}

AddPhoto.propTypes = {
    addMulti: PropTypes.bool.isRequired,
    addPhoto: PropTypes.func.isRequired,
    addPhotos: PropTypes.func.isRequired
};

export default AddPhoto;
