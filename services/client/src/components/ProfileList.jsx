import React from "react";
import PropTypes from "prop-types";
import "./ProfileList.css";
import 'react-alice-carousel/lib/alice-carousel.css';
import AliceCarousel from 'react-alice-carousel';


const UsersList = (props) => {
  var responsive = {
    0: {
        items: 1,
    },
    1024: {
        items: 3
    }
  }
  return (
    <div>
      {props.profiles.map((profile) => {
        return (
          <div key={profile.id}>
            <h2 className="username">{profile.username}</h2>
            <br />
            <AliceCarousel responsive={responsive}>
              {profile.photos.map((photo) => (
                <img className="alice-carousel" key={photo.id} src={`${process.env.REACT_APP_API_SERVICE_URL}/photos-view/`+ photo.path} />
              ))}
            </AliceCarousel>
            <br />
            <br />
          </div>
        );
      })}
    </div>
  );
};

UsersList.propTypes = {
  profiles: PropTypes.array.isRequired,
};

export default UsersList;
