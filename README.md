# Intro
Hi! I'm Aashna, a third year software engineering student at Carleton University. I built an image repository for the Shopify Dev Challenge January 2021 using Flask for the backend and React for the frontend. I started using Flask, using Docker, and learning how to build apis from scratch in September. I had very minimal experience with frontend development and no experience using React before this project. Given this, I started this project with the goal of learning something new that is backend related, getting a better idea of how the frontend uses backend apis so I can design them better, getting more familiar with Docker and Heroku, and learning React basics! I can proudly say I've achieved these goals and built something cool! In addition, I can proudly say that I have built secure apis with a ***96% unit test coverage***!

# Demo 
- The deployed heroku app link is still under construction, but please read the demo below:

### Explore Page
- Here users can view all of the profiles that exist on Candid., and scroll through everyone's public photos
![Explore Page](/resources/explore.JPG "Explore Page")

### My Profile
- Here the logged in user can view all of their public and private images
![My Profile](/resources/my_profile.jpg "My Profile")
- You can add a single photo with details by clicking the add button
![Add Photo](/resources/add_photo.JPG "Add Photo")
- You can quickly add multiple public or private photos
![Add Multiple Photos](/resources/add_multi_photo.JPG "Add Multiple Photos")
- You can select images by hovering over them and clicking on the checkmark
![Hover and Select](/resources/hover_image.JPG "Hover and Select")
- You can select images multiple images manually or click select all. Then click delete to delete the selected images
![Select Images](/resources/select_delete.JPG "Select Images")
![Select All Images](/resources/select_all.JPG "Select All Images")
- You can open an image and view its details
![Open Image](/resources/open_image.JPG "Select Images")

### Login / Register
- Login page
![Login](/resources/login.JPG "Login")
- Register page 
![Register](/resources/register.JPG "Register")
- These pages were built in the repo I forked off of


# Current APIs
- Apis for users and authenticated login (from original repo that I forked off of)
- Securely add, delete, and edit a single picture 
    - You can only add/edit/delete an image that you have access to. This is done through authenticating access token
    - Photos can have a title, caption, and can either be public/private photos. 
    - These fields can later be used to implement search and query features
- Get all of your images (public and private)
    - This is used to view your own profile
- Add multiple images to your profile
    - Swagger has a bug that inhibits you from selecting multiple pictures. To test this api, please use Postman
- Get all users' profile. This includes all of their public photos, username, and user id
- Get all of the public photos for a user
    - This would've been used to display another user's profile
- Get a single photo by photo id
- All above apis require tokens to ensure security and to ensure you can only view what you have access to
- The path at where photos are saved are not exposed in the api responses to ensure security. Instead you are given a filename
- Get an image file given the filename
    - This is used to display pictures in the frontend

## Other details
- All apis are unit tested with a total of 96% unit test coverage
- All apis are fully documented with various error code descriptions
- This backend was designed so it can easily be extended to include albums, search features, query filters, and sorting
- APIs currently exist to add more functionality in the frontend (edit image details, view other profiles, completing add multiple photos)
- Frontend is reponsive and easy to use

## Things that still need to be completed in the frontend
- Add multiple photos is mostly implemented
- Edit photo details (title, caption, etc.)
- View other user profiles

# How to set up locally
### Windows Users Only
```
0) Install Docker Desktop using the instructions below

Follow steps 1-5 here: https://docs.microsoft.com/en-us/windows/wsl/install-win10

Install Docker Desktop: https://docs.docker.com/docker-for-windows/wsl/

1) Install Notepad++: https://notepad-plus-plus.org/downloads/

2) Clone the project on to your local machine:

$ git clone https://gitlab.com/AashnaNarang/flask-react-auth.git

3) Navigate to the "flask-react-auth/services/users" folder in your File Explorer

4) Open "entrypoint.sh" using Notepad++

5) Go to Edit -> EOL conversion -> change from CRLF to LF

6) Save and exit the file

After that continue from STEP 2 below

```

## Initial Setup
```
0) Install Docker Desktop

1) Clone the project on to your local machine:

$ git clone https://gitlab.com/AashnaNarang/flask-react-auth.git

2) Run the following commands:

$ cd flask-react-auth
$ export REACT_APP_API_SERVICE_URL=http://localhost:5004

3) Build and start the Docker images:
$ docker-compose build
$ docker-compose up -d

4) Create and seed the database:
$ docker-compose exec api python manage.py recreate_db
$ docker-compose exec api python manage.py seed_db
```
You should now be able to ping: http://localhost:5004/ping    
You should be able to view the Swagger API doc at: http://localhost:5004/doc/    
And you should be able view the React app at: http://localhost:3007

To run tests and the linter please use the Docker commands explained below.

## Docker Workflow
Make sure you are in the flask-react-auth directory when running the following commands.

Run the tests:
```
$ docker-compose exec api python -m pytest "src/tests" -p no:warnings
```
Run the tests with coverage:
```
$ docker-compose exec api python -m pytest "src/tests" -p no:warnings --cov="src"
```
Run the client-side tests:
```
$ docker-compose exec client npm test
```
Run the client-side tests with coverage:
```
$ docker-compose exec client react-scripts test --coverage
```

## Other Commands
To stop the containers:
```
$ docker-compose stop
```
To bring down the containers:
```
$ docker-compose down
```
## Postgres

Want to access the database via psql?
```
$ docker-compose exec api-db psql -U postgres
```
Then, you can connect to the database and run SQL queries. For example:
```
# \c api_dev
# select * from users;
```

## Project Structure
```
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile.deploy
├── README.md
├── docker-compose.yml
├── release.sh
└── services
    ├── client  
    │
    ├── nginx
    │   └── default.conf
    │
    └── users
```

# Authentication with Flask, React, and Docker

[![pipeline status](https://gitlab.com/AashnaNarang/flask-react-auth/badges/master/pipeline.svg)](https://gitlab.com/AashnaNarang/flask-react-auth/commits/master)


# Sources
https://testdriven.io/courses/auth-flask-react/

